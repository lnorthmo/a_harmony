from asyncio import sleep
from web3 import AsyncWeb3
from web3.eth.async_eth import AsyncContract
from random import randint
import logging
from config import CustomConfig
from textwrap import dedent


async def estimate_fee(contract: AsyncContract, amount, address):
    value = await contract.functions.estimateSendFee(**{
        "_dstChainId": 116,
        "_toAddress": bytes.fromhex(address[2:].lower()),
        "_amount": amount,
        "_useZro": True,
        "_adapterParams": b'\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\xa1 ' # noqa
    }).call()
    return value[0]


async def bridge(web3: AsyncWeb3, contract: AsyncContract, address, private_key):  # noqa
    address = web3.to_checksum_address(address)
    for i in range(CustomConfig.trans_quantity):
        try:
            balance_before = web3.from_wei(await web3.eth.get_balance(address), 'ether')
            amount = randint(CustomConfig.min_amount, CustomConfig.max_amount)
            value = amount + await estimate_fee(contract, amount, address)
            transaction = await contract.functions.sendFrom(**{
                "_from": address,
                "_dstChainId": 116,
                "_toAddress": bytes.fromhex(address[2:].lower()),
                "_amount": amount,
                "_refundAddress": address,
                "_zroPaymentAddress": '0x0000000000000000000000000000000000000000',
                "_adapterParams": b'\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\xa1 ' # noqa
            }).build_transaction({
                'from': address,
                'gasPrice': await web3.eth.gas_price,
                'gas': 500000,
                'nonce': await web3.eth.get_transaction_count(address),
                'value': value
            })
            transaction = web3.eth.account.sign_transaction(transaction, private_key=private_key)

            tx_hash = await web3.eth.send_raw_transaction(transaction.rawTransaction)
            txstatus = await web3.eth.wait_for_transaction_receipt(tx_hash)
            if txstatus.status != 1:
                tx = await web3.eth.get_transaction(tx_hash)
                logging.warning(dedent(f'''
                transaction for {address} was not success\n
                probably you out of tokens\n 
                hash: {tx_hash.hex()}\n
                transaction: {tx}\n
                {'=' * 200}'''))
            else:
                await sleep(0.2)
                balance_after = web3.from_wei(await web3.eth.get_balance(address), 'ether')
                logging.info(dedent(f"""
                            Bridge success for {address}\n
                            balance before: {balance_before}\n
                            balance after:  {balance_after}\n
                            tx hash: {tx_hash.hex()}
                            {'=' * 200} """))

        except Exception as e:
            logging.error(dedent(f'''ERROR ACCUSED: \n {e} {'=' * 200} '''))
        await sleep(randint(CustomConfig.min_trans_delay, CustomConfig.max_amount))
