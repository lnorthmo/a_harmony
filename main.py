import asyncio
import yaml
from web3 import AsyncWeb3, AsyncHTTPProvider
from web3.eth.async_eth import AsyncContract
from config import BSCConfig
from services import harmony
import logging.config


with open('log_config.yaml', 'r') as stream:
    log_config = yaml.load(stream, Loader=yaml.FullLoader)
logging.config.dictConfig(log_config)

async def main(web3: AsyncWeb3, contacrt: AsyncContract, wallets: list):  # noqa
    tasks = []
    for wallet in wallets:
        address, private_key = wallet.split(';')
        tasks.append(harmony.bridge(web3, contacrt, address, private_key))
    await asyncio.gather(*tasks)


if __name__ == '__main__':
    web3 = AsyncWeb3(AsyncHTTPProvider(BSCConfig.rpc_url))
    contract = web3.eth.contract(BSCConfig.contract_address, abi=BSCConfig.bas_harm_abi)
    with open('wallets.txt', 'r') as f:
        wallets = f.read().splitlines()
    asyncio.run(main(web3, contract, wallets))
